const path = require('path');
const packageName = require('./package.json').name;

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? 'http://static.system.rainbowinpaper.cn/':'/',
  devServer: {
    port: 10200,
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    open: true,
    host: '127.0.0.1',
    proxy: {
      '/api/': {
        target: 'http://123.60.137.241/',
        // target: 'http://127.0.0.1:8000/',
        changeOrigin: true,
        pathRewrite: {
          '^/api/': ''
        }
      }
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        '@src': path.resolve(__dirname, 'src'),
        '@pages': path.resolve(__dirname, 'src/pages'),
      }
    },
    output: {
      library: "ManageMicroApp",
      libraryTarget: 'umd',
      jsonpFunction: `webpackJsonp_${packageName}`,
    }
  }

}
