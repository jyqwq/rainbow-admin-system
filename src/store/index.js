import Vue from 'vue';
import Vuex from 'vuex';
import Menu from '@pages/Menu/store';
import Model from '@pages/Model/store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Menu,
    Model,
  }
});
