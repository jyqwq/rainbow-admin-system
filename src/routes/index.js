import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@pages/home')
  },
  // {
  //   path: '/model',
  //   name: 'model',
  //   component: () => import('@pages/Model')
  // },
  // {
  //   path: '/menu/list',
  //   name: 'list',
  //   component: () => import('@pages/Menu')
  // }
];
const router = new VueRouter({
  base: window.__POWERED_BY_QIANKUN__ ? '/manage/' : '/',
  mode: 'history',
  routes,
});
router.onError((error) => {
  const pattern = /Loading chunk (\d)+ failed/g;
  const isChunkLoadFailed = error.message.match(pattern);
  if (isChunkLoadFailed) {
    // 用路由的replace方法，并没有相当于F5刷新页面，失败的js文件并没有从新请求，
    // 会导致一直尝试replace页面导致死循环，
    // 而用 location.reload 方法，相当于触发F5刷新页面，
    // 虽然用户体验上来说会有刷新加载察觉，
    // 但不会导致页面卡死及死循环，从而曲线救国解决该问题
    location.reload();
  }
})
// router.beforeEach((to, from, next) => {
//   console.log(router.options.isAddDynamicMenuRoutes, 'router.options.isAddDynamicMenuRoutesrouter.options.isAddDynamicMenuRoutes');
//   // 如果已经添加动态路由，next
//   if (router.options.isAddDynamicMenuRoutes) {
//     next();
//   } else {
//     // 获取路由表数据
//     const menus = JSON.parse(localStorage.getItem('menus'));
//     if (menus) {
//       // 递归处理生成动态路由表
//       fnAddDynamicMenuRoutes(menus.route);
//       router.options.isAddDynamicMenuRoutes = true;
//       next();
//     } else {
//       next()
//     }
//   }
// })
const menus = JSON.parse(localStorage.getItem('menus'));
if (menus) {
  // 递归处理生成动态路由表
  fnAddDynamicMenuRoutes(menus.route);
}
/**
 * 添加动态(菜单)路由
 * @param menuList
 * @param dynamicRoutes
 */
function fnAddDynamicMenuRoutes(menuList = [], dynamicRoutes = []) {
  let temp = []
  for (let i = 0; i < menuList.length; i++) {
    if (!menuList[i].url.includes("/manage")) continue;
    if (menuList[i].children && menuList[i].children.length >= 1) {
      temp = temp.concat(menuList[i].children)
    } else if (menuList[i].path) {
      console.log(menuList[i].url, 'pathpathpathpathpath');
      let route = {
        path: menuList[i].url.replace("/manage",""),
        component: null,
        name: menuList[i].name,
        meta: {
          menuId: menuList[i].id,
          title: menuList[i].name,
          isDynamic: true,
          isTab: true,
        }
      }
      try {
        route['component'] = () => import(`@/pages/${menuList[i].path}`) || null;
        // route['component'] = (resolve) => require([`@/pages/${menuList[i].path}`], resolve) || null;
      } catch (e) {
        console.log(e);
      }
      dynamicRoutes.push(route)
    }
  }
  if (temp.length >= 1) {
    fnAddDynamicMenuRoutes(temp, dynamicRoutes)
  } else {
    routes.push(...dynamicRoutes)
    console.log(routes, 'dynamicRoutesdynamicRoutesdynamicRoutes');
    router.addRoutes(routes)
    console.log('%c!<-------------------- 动态(菜单)路由 s -------------------->', 'color:blue')
    console.log('%c!<-------------------- 动态(菜单)路由 e -------------------->', 'color:blue')
  }
}

export default router;
