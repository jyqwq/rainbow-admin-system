import {getters} from "@pages/Menu/getters";
import {actions} from "@pages/Menu/actions";
import {mutations} from "@pages/Menu/mutations";


const state = () => ({
    name: 'Menu',
    loading: false,
    data: [],
    cacheData: [],
    editingKey:'',
    modelVisible:false
})


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
