import {
  GET_LIST_ACTION,
  SET_LIST_MUTATION,
  SET_CACHE_LIST_MUTATION,
  DELETE_MENU_ACTION,
  ADD_MENU_ACTION,
  UPDATE_MENU_ACTION,
  SET_MODEL_MUTATION
} from './constants';
import { getMenu, delMenu, addNewMenu, upMenu } from '@/apis/manage';
import { message } from 'ant-design-vue';

export const actions = {
  async [GET_LIST_ACTION]({ commit }) {
    const {
      data: { data }
    } = await getMenu();
    await commit(SET_LIST_MUTATION, data);
    await commit(SET_CACHE_LIST_MUTATION, data);
  },
  async [DELETE_MENU_ACTION]({ dispatch }, id) {
    const res = await delMenu({ id });
    if (res.status === 200) {
      message.success('删除成功');
      await dispatch(GET_LIST_ACTION);
    } else {
      message.error('删除失败');
    }
  },
  async [ADD_MENU_ACTION]({ commit, dispatch }, form) {
    const res = await addNewMenu(form);
    if (res.status === 200) {
      message.success('添加成功');
      await commit(SET_MODEL_MUTATION);
      await dispatch(GET_LIST_ACTION);
    } else {
      message.error('添加失败');
    }
  },
  async [UPDATE_MENU_ACTION]({ dispatch }, form) {
    const res = await upMenu(form);
    if (res.status === 200) {
      message.success('修改成功');
    } else {
      message.error('修改失败');
    }
    await dispatch(GET_LIST_ACTION);
  }
};
