import {
  SET_LIST_MUTATION,
  SET_CACHE_LIST_MUTATION,
  SET_EDITING_KEY_MUTATION,
  SET_LOADING_MUTATION,
  SET_MODEL_MUTATION
} from './constants';

export const mutations = {
  [SET_LIST_MUTATION](state, payload) {
    state.data = payload;
  },
  [SET_CACHE_LIST_MUTATION](state, payload) {
    state.cacheData = payload;
  },
  [SET_EDITING_KEY_MUTATION](state, payload) {
    state.editingKey = payload;
  },
  [SET_LOADING_MUTATION](state, payload) {
    state.loading = payload;
  },
  [SET_MODEL_MUTATION](state, payload) {
    state.modelVisible = payload;
  }
};
