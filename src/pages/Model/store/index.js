import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';

const state = () => ({
  name: 'Model',
  loading: false,
  listData: [],
  cacheData: [],
  editingKey: '',
  activeModel: {},
  modelVisible: false
});

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
