import {
  GET_LIST_ACTION,
  SET_LIST_MUTATION,
  // SET_CACHE_LIST_MUTATION,
  // DELETE_MENU_ACTION,
  // ADD_MENU_ACTION,
  UPDATE_MODEL_ACTION,
  SET_LOADING_MUTATION,
  SET_MODEL_MUTATION
} from './constants';
import { getMenuTemplate, upMenuTemplate } from '@/apis/model';
import { message } from 'ant-design-vue';

export const actions = {
  async [GET_LIST_ACTION]({ commit }) {
    commit(SET_LOADING_MUTATION, true);
    const {
      data: { data }
    } = await getMenuTemplate();
    commit(SET_LIST_MUTATION, data);
    commit(SET_LOADING_MUTATION, false);
    // await commit(SET_CACHE_LIST_MUTATION, data);
  },
  // async [DELETE_MENU_ACTION] ({ dispatch }, id) {
  //     const res = await delMenu({id})
  //     if(res.status===200){
  //         message.success('删除成功');
  //         await dispatch(GET_LIST_ACTION)
  //     }else {
  //         message.error('删除失败');
  //     }
  // },
  // async [ADD_MENU_ACTION] ({ commit, dispatch }, form) {
  //     const res = await addNewMenu(form)
  //     if(res.status===200){
  //         message.success('添加成功');
  //         await commit(SET_MODEL_MUTATION);
  //         await dispatch(GET_LIST_ACTION);
  //     }else {
  //         message.error('添加失败');
  //     }
  // },
  async [UPDATE_MODEL_ACTION]({ commit, dispatch }, form) {
    const res = await upMenuTemplate(form);
    if (res.status === 200 && res.data.state === 200) {
      message.success('修改成功');
      await commit(SET_MODEL_MUTATION, false);
      await dispatch(GET_LIST_ACTION);
    } else {
      message.error(res.data.message || '修改失败');
    }
  }
};
