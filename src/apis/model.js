import { get, post } from './index';
// 登录请求
export const getMenuTemplate = (params) =>
  get('/system/menu/getMenuTemplate', params);
export const addMenuTemplate = (params) =>
  post('/system/menu/addMenuTemplate', params);
export const delMenuTemplate = (params) =>
  post('/system/menu/delMenuTemplate', params);
export const upMenuTemplate = (params) =>
  post('/system/menu/upMenuTemplate', params);
