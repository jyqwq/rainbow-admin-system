import { get, post } from './index';
// 登录请求
export const getMenu = (params) => get('/system/menu/getMenu', params);
export const addNewMenu = (params) => post('/system/menu/addMenu', params);
export const delMenu = (params) => post('/system/menu/delMenu', params);
export const upMenu = (params) => post('/system/menu/upMenu', params);
