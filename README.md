# 项目说明

## 生成环境

```shell
npm install yarn -g
yarn install
```

## 启动项目

```shell
yarn serve
```

## 打包项目

```shell
yarn build
```

## 生成新页面

```shell
npm run generate
```

## 新页面结构

- actions.js

  处理异步模块

  ```js
  import {
      GET_LIST_ACTION, // 常量引入
  } from './constants'
  import { getMenu } from "@/apis/manage"; // api引入
  import { message } from 'ant-design-vue' // 组件
  
  export const actions = {
    	// 函数名为引入的常量 所有的api commit dispatch都需要await
      async [GET_LIST_ACTION] ({ commit, dispatch }, params) {
          const {data:{data}}=await getMenu()
          await commit(SET_LIST_MUTATION,data)
          await commit(SET_CACHE_LIST_MUTATION,data)
      },
  }
  ```

- constants.js

  常量模块

  ```js
  // 所有常量需备注功能 action模块的以_ACTION结尾 mutation模块的以_MUTATION结尾
  //获取列表
  export const GET_LIST_ACTION = 'GET_LIST_ACTION'
  ```

- getters.js

  数据处理模块

- index.vue

  页面

  ```vue
  <template>
  </template>
  <script>
  	import { mapState, mapActions, mapMutations } from 'vuex'
    import {
      GET_LIST_ACTION,
      SET_LIST_MUTATION
    } from './constants'
    export default {
        /**
         * computed会把vuex store里面的值拿到当前页面上来
         */
      computed: mapState({
        loading: state => state.Menu.loading,
        data: state => state.Menu.data,
      }),
        methods: {
      /**
       * mapMutations把vuex Mutations里面的方法拿到当前页面来 
       * 这是用来更新store里面的值 同步执行
       */
      ...mapMutations({
        setData:`Menu/${SET_LIST_MUTATION}`,
      }),
      /**
       * mapActions把vuex actions里面的方法拿到当前页面来 
       * 这是用来处理异步以及复杂业务 通过await实现同步执行
       */
      ...mapActions({
        getListAction:`Menu/${GET_LIST_ACTION}`,
      }),
      created() {
        this.getListAction();
      },
    }
  </script>
  ```

- mutations.js

  更新store模块

  ```js
  import {
      SET_LIST_MUTATION,
  } from './constants'
  
  export const mutations = {
      [SET_LIST_MUTATION] (state,payload) {
          state.data = payload;
      },
  }
  ```

- store.js

  store模块

  ```js
  import {getters} from "@pages/menu/getters";
  import {actions} from "@pages/menu/actions";
  import {mutations} from "@pages/menu/mutations";
  
  
  const state = () => ({
      name: 'Menu',
      data: [],
  })
  
  
  export default {
      namespaced: true,
      state,
      getters,
      actions,
      mutations
  }
  
  ```

  
