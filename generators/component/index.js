/**
 * Component Generator
 */

const componentExists = require('../utils/componentExists');

module.exports = {
    description: '添加一个新组件',
    prompts: [
        {
            type: 'input',
            name: 'name',
            message: '组件名称是什么？',
            default: 'Form',
            validate: value => {
                if (/.+/.test(value)) {
                    return componentExists(value)
                        ? '组件已经存在'
                        : true;
                }

                return 'The name is required';
            },
        },
    ],
    actions: () => {

        const actions = [
            {
                type: 'add',
                path: '../src/components/{{properCase name}}/index.vue',
                templateFile: './component/page.js.hbs',
                abortOnFail: true,
            },
            {
                type: 'add',
                path: '../src/components/{{properCase name}}/style.less',
                templateFile: './component/style.less.hbs',
                abortOnFail: true,
            },
        ];

        actions.push({
            type: 'prettify',
            path: '/components/',
        });

        return actions;
    },
};
