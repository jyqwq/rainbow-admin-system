/**
 * Container Generator
 */

const componentExists = require('../utils/componentExists');

module.exports = {
    description: '添加一个新页面',
    prompts: [
        {
            type: 'input',
            name: 'name',
            message: '页面名称是什么？',
            default: 'Form',
            validate: value => {
                if (/.+/.test(value)) {
                    return componentExists(value)
                        ? '页面已经存在'
                        : true;
                }

                return 'The name is required';
            },
        },
        {
            type: 'confirm',
            name: 'wantStore',
            default: true,
            message:
                '需要使用Store吗？',
        },
    ],
    actions: data => {

        const actions = [
            {
                type: 'add',
                path: '../src/pages/{{properCase name}}/index.vue',
                templateFile: './page/page.js.hbs',
                abortOnFail: true,
            },
            {
                type: 'add',
                path: '../src/pages/{{properCase name}}/style.less',
                templateFile: './page/style.less.hbs',
                abortOnFail: true,
            },
        ];

        if (data.wantStore) {
            // Constants
            actions.push({
                type: 'add',
                path: '../src/pages/{{properCase name}}/constants.js',
                templateFile: './page/constants.js.hbs',
                abortOnFail: true,
            });

            // Actions
            actions.push({
                type: 'add',
                path: '../src/pages/{{properCase name}}/actions.js',
                templateFile: './page/actions.js.hbs',
                abortOnFail: true,
            });

            // Mutations
            actions.push({
                type: 'add',
                path: '../src/pages/{{properCase name}}/mutations.js',
                templateFile: './page/mutations.js.hbs',
                abortOnFail: true,
            });

            // Getters
            actions.push({
                type: 'add',
                path: '../src/pages/{{properCase name}}/getters.js',
                templateFile: './page/getters.js.hbs',
                abortOnFail: true,
            });

            // Store
            actions.push({
                type: 'add',
                path: '../src/pages/{{properCase name}}/store.js',
                templateFile: './page/store.js.hbs',
                abortOnFail: true,
            });

            // store
            actions.push({
                type: 'modify',
                path: '../src/store/index.js',
                pattern: /(import .+ from '@pages\/.+\/store';\n)(?!import .+ from '@pages\/.+\/store';\n)/g,
                templateFile: './page/store.import.hbs',
            });
            actions.push({
                type: 'modify',
                path: '../src/store/index.js',
                pattern: /([ ]{4}[A-Z][a-zA-Z]*,\n)(?![ ]{4}[A-Z][a-zA-Z]*,\n)/g,
                templateFile: './page/store.module.hbs',
            });
        }

        actions.push({
            type: 'prettify',
            path: '/pages/',
        });

        return actions;
    },
};
